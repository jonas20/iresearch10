//
//  IRPersonmentphotoViewController.h
//  iResearch10
//
//  Created by jonas hu on 12-11-21.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IRPersonmentphotoViewController : UIViewController


@property (nonatomic,copy)NSString *linkNameString;


@property(nonatomic,strong)UIView *exifUIView;
@property(nonatomic,strong)UILabel *exifText;

@end
