//
//  IRPersonmentphotoViewController.m
//  iResearch10
//
//  Created by jonas hu on 12-11-21.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import "IRPersonmentphotoViewController.h"
#import "IRFoundation.h"


@implementation IRPersonmentphotoViewController
@synthesize linkNameString;



@synthesize  exifUIView;
@synthesize  exifText;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor blackColor];
    
    UIImage *image=[UIImage imageNamed:[linkNameString  stringByAppendingString:@".jpg" ]];
    UIImageView *imageView=[[UIImageView alloc] initWithImage:image];
    
    imageView.frame=CGRectMake(0, 0, image.size.width, image.size.height);

    [self.view addSubview:imageView];

    
    
    
    
    //添加EXIF信息 VIEW
    
    CGRect exifCGRframe=CGRectMake(10, self.view.bounds.size.height-60, 0, 0);
    
    exifUIView=[[UIView alloc] initWithFrame:exifCGRframe];
    exifUIView.backgroundColor=[UIColor blackColor];
    //exifUIView.alpha=0.4f;
    
    
    //exifText=[[UILabel alloc] initWithFrame:CGRectMake(exifCGRframe.origin.x+3, exifCGRframe.origin.y, exifCGRframe.size.width, exifCGRframe.size.height)];
    
    exifText=[[UILabel alloc] init];
    
    
    exifText.backgroundColor=[UIColor clearColor];
    exifText.textColor=[UIColor whiteColor];
    exifText.font=[UIFont systemFontOfSize:12.0f];
    exifText.numberOfLines=0; // 行数，0不限制
    exifText.textAlignment=UITextAlignmentLeft;
    //exifText.lineBreakMode=UILineBreakModeCharacterWrap; //换行方式 字母换行
    
    
    [self.view addSubview:exifUIView];
    [self.view addSubview:exifText];
    
    exifUIView.hidden=YES;
    exifText.hidden=YES;

    
    [self infoShow];
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self infoShow];
}


-(void)infoShow
{
    
    NSString *fullPath = [[NSBundle mainBundle] pathForResource:linkNameString ofType:@"txt"];
    
    NSString *text = [NSString stringWithContentsOfFile:fullPath
                                               encoding:NSUTF8StringEncoding
                                                  error:nil];
    
    
    

    
    exifText.text=[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    CGSize size=[exifText.text sizeWithFont:[UIFont systemFontOfSize:12.0f]
                          constrainedToSize:CGSizeMake(self.view.frame.size.width, 1000000) lineBreakMode:UILineBreakModeCharacterWrap];
    
    
    
    //exifText.frame=CGRectMake(exifUIView.frame.origin.x, 400- size.height, size.width, size.height);
    //exifUIView.frame=CGRectMake(exifUIView.frame.origin.x, 400- size.height, size.width,size.height);
    
    
    
    exifText.frame=CGRectMake(0, 400- size.height, size.width, size.height);
    exifUIView.frame=CGRectMake(0, 400- size.height-3, size.width,size.height+6);
    
    
    
    [exifUIView setHidden:!exifUIView.hidden];
    //[exifText setHidden:!exifText.hidden];
    
    if (exifText.hidden) {
        
        exifText.alpha = 0.0;
        exifText.hidden=NO;
        
        exifUIView.alpha = 0.0;
        exifUIView.hidden=NO;
        
        [UIView animateWithDuration:.5 delay:0.0
                            options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             exifText.alpha = 1.0;
                             exifUIView.alpha=0.4;
                             //imageView.center = CGPointMake(500.0, 512.0);
                         }
                         completion:^(BOOL finished){
                             [UIView animateWithDuration:.75
                                              animations:^{
                                                  //newImageView.center = CGPointMake(500.0, 512.0);
                                                  //exifText.hidden=NO;
                                              }];
                         }];
    }
    else
    {
        exifUIView.hidden=YES;
        //隐藏
        [UIView animateWithDuration:0.35 delay:0.0
                            options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                         animations:^(void)
         {
             exifText.alpha = 0.0f;
         }
                         completion:^(BOOL finished)
         {
             exifText.hidden = YES;
         }
         ];
    }
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
