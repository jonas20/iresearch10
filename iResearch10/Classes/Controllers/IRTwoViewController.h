//
//  IRTwoViewController.h
//  iResearch10
//
//  Created by jonas hu on 12-11-21.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IRTwoViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>



@property(nonatomic,strong) NSArray         *keysAll;
@property(nonatomic,strong) NSDictionary    *dicDepatrment;

@property(nonatomic,strong) UITableView     *tableViewDepart;

@end
