//
//  IRTwoViewController.m
//  iResearch10
//
//  Created by jonas hu on 12-11-21.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import "IRTwoViewController.h"
#import "IRPersonnelViewController.h"
#import "IRFoundation.h"

@implementation IRTwoViewController


@synthesize keysAll;
@synthesize dicDepatrment;
@synthesize tableViewDepart;



#pragma mark -表格数据源及代理


//行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    

   return [[dicDepatrment objectForKey:keysAll[section]] count];

}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return keysAll.count;
    
}

-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return keysAll[section];
}


//每个Cell展现的内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString  *CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell==nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.selectionStyle=UITableViewCellSelectionStyleBlue;//选中Cell后背景
    }
    
    if([keysAll count ]>indexPath.section)
    {
        NSArray *array=[dicDepatrment objectForKey:keysAll[indexPath.section]];
        
        
        
        cell.textLabel.text=array[indexPath.row];
        
        cell.imageView.image=[UIImage imageNamed:@"structure.png"];
        
        
    }
    
    return cell;
}



//选中某一个Cell 要执行的事件
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([keysAll count ]>indexPath.section)
    {
        NSArray *array=[dicDepatrment objectForKey:keysAll[indexPath.section]];

    
        IRPersonnelViewController *personnelViewController=[[IRPersonnelViewController alloc] init];
    
    
        personnelViewController.areaFile=keysAll[indexPath.section];
        personnelViewController.depatment=array[indexPath.row];
        
        [self.navigationController pushViewController:personnelViewController animated:YES ];
    }
}



#pragma -mark 生命周期及内存管理

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Department"
                                                     ofType:@"plist"];
    
    dicDepatrment = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    
    keysAll = [[dicDepatrment allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    
    //添加一个表格
    
    tableViewDepart= [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, ViewHeight) style:UITableViewStyleGrouped];
    
    tableViewDepart.delegate=self;
    
    tableViewDepart.dataSource=self;
    //tableViewCataLog.separatorColor=[UIColor clearColor];  //表间距背景色
    tableViewDepart.backgroundColor=[UIColor clearColor];
    tableViewDepart.backgroundView=nil;
    
    [self.view addSubview:tableViewDepart];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
