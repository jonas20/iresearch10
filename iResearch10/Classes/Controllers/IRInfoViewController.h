//
//  IRInfoViewController.h
//  iResearch10
//
//  Created by jonas hu on 12-11-27.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IRInfoViewController : UIViewController


@property(nonatomic,strong)UIWebView *webView;
@property(nonatomic,assign)BOOL iswebView;

@property(nonatomic,copy)NSString *fileName;
@property(nonatomic,assign)BOOL isMovie;
@property(nonatomic,copy)NSString *infoURLString;
@end
