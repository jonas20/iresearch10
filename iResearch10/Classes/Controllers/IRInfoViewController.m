//
//  IRInfoViewController.m
//  iResearch10
//
//  Created by jonas hu on 12-11-27.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import "IRInfoViewController.h"
#import "IRFoundation.h"

@implementation IRInfoViewController
@synthesize infoURLString;
@synthesize isMovie;
@synthesize fileName;
@synthesize iswebView;
@synthesize webView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)PalyVodie
{
    
    if (!isMovie) {
        return;
    }
    
    if (iswebView) {
        
        [webView removeFromSuperview];
        iswebView=NO;
    }
    else
    {
        
        webView=[[UIWebView alloc] init];
        
        webView.frame=CGRectMake(10, 50, 300,170);
        
        webView.alpha=.9f;
        webView.backgroundColor=[UIColor blackColor];
        
        [self.view addSubview:webView];
        
        
        
        NSURL *URL=[[NSURL alloc] initWithString:[NSString stringWithFormat:@"http://%@/iresearch10/v.aspx?file=%@",Domain,fileName] ];
        
        NSURLRequest *urlRequest=[NSURLRequest requestWithURL:URL];
        
    
        
        webView.autoresizingMask = (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
//        webView.delegate = self;

        
        //webView.allowsInlineMediaPlayback = NO;
        //webView.mediaPlaybackRequiresUserAction = NO;
        
        [webView loadRequest:urlRequest];
        iswebView=YES;
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
//    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight];

    [self PalyVodie];
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}



-(void)viewDidAppear:(BOOL)animated
{
     [self PalyVodie];   
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
