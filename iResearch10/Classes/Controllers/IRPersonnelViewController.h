//
//  IRPersonnelViewController.h
//  iResearch10
//
//  Created by jonas hu on 12-11-21.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//  员工列表

#import <UIKit/UIKit.h>

@interface IRPersonnelViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,copy)NSString       *areaFile;
@property(nonatomic,copy)NSString       *depatment;

@property(nonatomic,strong)UITableView  *tableViewPersonnel;

@property(nonatomic,strong)NSArray      *linkManArray;

@end
