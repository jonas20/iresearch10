//
//  IRLoginViewController.m
//  iResearch10
//
//  Created by jonas hu on 13-1-4.
//  Copyright (c) 2013年 jonas hu. All rights reserved.
//

#import "IRLoginViewController.h"
#import "ASIHTTPRequest.h"

#import "IROneViewController.h"
#import "IRTwoViewController.h"
#import "IRFoundation.h"
#import "IRViewController.h"

@implementation IRLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma -mark  生命周期管理

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    
    
    NSUserDefaults *userSaveDefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSDictionary *userInfo=[userSaveDefaults objectForKey:@"userinfosave"];

    if (userInfo!=nil) {
        
        self.userNameString=[userInfo objectForKey:@"user"];
        self.passwordString=[userInfo objectForKey:@"password"];
        
        [self loginVerification];
        
    }
    else
    {
  
        [self.userNameText becomeFirstResponder];
    }
    
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchUpinside:(id)sender {
    
    [self.userNameText resignFirstResponder];
    [self.passwordText resignFirstResponder];
    
}

- (void)viewDidUnload {

    [self setUserNameText:nil];
    [self setPasswordText:nil];
    [self setLoginButton:nil];
    [super viewDidUnload];
}
- (IBAction)userNameonExit:(id)sender {
    
    [self.passwordText becomeFirstResponder];
}

- (IBAction)passwordonExit:(id)sender {
    
    [sender resignFirstResponder];

}

- (IBAction)loginOnclick:(id)sender {
    
     self.userNameString=[self.userNameText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
     self.passwordString=[self.passwordText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([self.userNameString isEqualToString:@""]) {
        
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"" message:@"用户名不能为空" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil ];
        
        [alertView show];
        return;
        
    }
    
    if([self.passwordString isEqualToString:@""]) {
        
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"" message:@"密码不能为空" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil ];
        
        [alertView show];
        return;
    }

    [self.userNameText resignFirstResponder];
    [self.passwordText resignFirstResponder];


    self.loginButton.enabled=NO;
    
    //验证用户名密码
    [self loginVerification];
    
}


#pragma -mark 自定义方法
-(void)loginVerification
{
    
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"http://crm.iresearch.com.cn/webinterface/usercheck.aspx?act=pscheck&user=%@&password=%@",self.userNameString,self.passwordString]];
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    
    [request setDelegate:self];
	//[request setShowAccurateProgress:YES];
	[request startAsynchronous];
    

    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
	[self.view addSubview:HUD];
	
	HUD.delegate = self;
	HUD.labelText = @"正在验证...";

    [HUD show:YES];
	
   
}

//进入主视图
-(void)mainViewControll
{
    
    NSArray *tabBarItemTitle=[[NSArray alloc] initWithObjects:@"十年献礼",@"艾瑞大家庭",@"年会随手拍",nil];
    
    NSArray *tabBarItemImage=[[NSArray alloc] initWithObjects:@"one.png",@"two.png",@"three.png",nil];
    
    IROneViewController *oneviewController=[[IROneViewController alloc] init];
    
    oneviewController.view.backgroundColor=Color_BackgroundColor;
    
    oneviewController.tabBarItem.title=tabBarItemTitle[0];
    oneviewController.tabBarItem.image=[UIImage imageNamed:tabBarItemImage[0]];
    
    
    
    
    IRTwoViewController *twoviewController=[[IRTwoViewController alloc] init];
    twoviewController.view.backgroundColor=Color_BackgroundColor;
    
    UINavigationController *twoNavigation=[[UINavigationController alloc] initWithRootViewController:twoviewController];
    twoNavigation.tabBarItem.title=tabBarItemTitle[1];
    twoNavigation.tabBarItem.image=[UIImage imageNamed:tabBarItemImage[1]];
    
    twoNavigation.navigationBar.tintColor=Color_NavigationBarTint;
    
    twoviewController.navigationItem.title=tabBarItemTitle[1];
    
    
    
    
    IRViewController *threeviewController=[[IRViewController alloc] init];
    
    
    UINavigationController *threeNavigation=[[UINavigationController alloc] initWithRootViewController:threeviewController];
    threeNavigation.tabBarItem.title=tabBarItemTitle[2];
    threeNavigation.tabBarItem.image=[UIImage imageNamed:tabBarItemImage[2]];
    threeNavigation.navigationBar.tintColor=Color_NavigationBarTint;
    
    threeviewController.navigationItem.title=tabBarItemTitle[2];
    
    
    
    self.mainTabBarController=[[UITabBarController alloc] init];
    self.mainTabBarController.viewControllers=[[NSArray alloc] initWithObjects:oneviewController,twoNavigation,threeNavigation, nil];
    
    [self presentModalViewController:self.mainTabBarController animated:YES];
}

#pragma -mark ASIHTTPRequest下载代理
-(void) requestFinished:(ASIHTTPRequest *)request{
    
    [HUD removeFromSuperview];
    HUD = nil;

    NSError *error = [request error];
        
    if (!error) {
        
        NSString *response = [request responseString];
        
        if(![response isEqualToString:@"OK"])
        {            
            UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"" message:@"用户名或密码错误！" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alertView show];

        }
        else
        {
            
            NSDictionary *userInfoDictionary=[[NSDictionary alloc] init];
            userInfoDictionary=@{@"user":self.userNameString,@"password":self.passwordString};
                        
            NSUserDefaults *userSaveDefaults=[NSUserDefaults standardUserDefaults];
            [userSaveDefaults setObject:userInfoDictionary forKey:@"userinfosave"];
            
            
            //登录成功
            [self mainViewControll];
        }
        
    }
    else
    {
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"" message:@"登录失败！" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
    }
    
    self.loginButton.enabled=YES;

    
}


@end
