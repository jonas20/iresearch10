//
//  IRLoginViewController.h
//  iResearch10
//
//  Created by jonas hu on 13-1-4.
//  Copyright (c) 2013年 jonas hu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface IRLoginViewController : UIViewController<MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    
}

@property(nonatomic,copy)NSString *userNameString;
@property(nonatomic,copy)NSString *passwordString;

@property(strong,nonatomic) UITabBarController  *mainTabBarController;

@property (strong, nonatomic) IBOutlet UITextField *userNameText;
@property (strong, nonatomic) IBOutlet UITextField *passwordText;

@property (strong, nonatomic) IBOutlet UIButton *loginButton;


- (IBAction)touchUpinside:(id)sender;
- (IBAction)userNameonExit:(id)sender;
- (IBAction)passwordonExit:(id)sender;
- (IBAction)loginOnclick:(id)sender;

@end
