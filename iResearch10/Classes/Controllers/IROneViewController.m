//
//  IROneViewController.m
//  iResearch10
//
//  Created by jonas hu on 12-11-21.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import "IROneViewController.h"


@implementation IROneViewController

#define PageCount 2


#pragma -mark XLCycleScrollView 代理

- (NSInteger)numberOfPages
{
    return PageCount;
}

- (UIView *)pageAtIndex:(NSInteger)index
{
    if (index < 0||index >=PageCount )
    {
        return nil;
    }
    
//    UIImageView *imageView=[[UIImageView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 411)];
    
    imageView.contentMode=UIViewContentModeScaleAspectFit; //缩放不变形
    
    imageView.image=[self imageFile:index];
    
    
    return imageView;
    
}


- (void)didClickPage:(XLCycleScrollView *)csView atIndex:(NSInteger)index
{
}





#pragma -mark 自定义方法


-(UIImage *)imageFile:(NSInteger)index
{
    
    
    NSString *filename=[[NSString alloc] initWithFormat:@"ire_p%d",index];
    
    //图片引用标准方式，效率高
    return [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:filename ofType:@"jpg"]];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    //添加滚动视图
    XLCycleScrollView *csView = [[XLCycleScrollView alloc] initWithFrame:self.view.bounds];
    csView.delegate = self;
    csView.datasource = self;
    [self.view addSubview:csView];
    

      


}

//
//-(void)playMovie:(NSString *)fileName{
////    
////    //视频文件路径
////    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"m4v"];
////    //视频URL
////    NSURL *url = [NSURL fileURLWithPath:path];
////    //视频播放对象
////    
//    
//     NSURL *URL=[[NSURL alloc] initWithString:@"http://192.168.2.41:8086/iresearch10/photo/jonas.mp4" ];
//
//    MPMoviePlayerController *movie = [[MPMoviePlayerController alloc] initWithContentURL:URL];
//    
//    movie.controlStyle = MPMovieControlStyleNone;
//    [movie setMovieSourceType:MPMovieSourceTypeFile];
//    //[movie setScalingMode:MPMovieScalingModeFill]
//    
//    movie.view.frame=CGRectMake(0, 0, 320, 300);
//    [movie.view setFrame:self.view.bounds];
//    movie.initialPlaybackTime = -1;
//    
//    [self.view addSubview:movie.view];
//    
//    // 注册一个播放结束的通知
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(jonas:)
//                                                 name:MPMoviePlayerPlaybackDidFinishNotification
//                                               object:movie];
//    [movie play];
//}
//
//#pragma mark -------------------视频播放结束委托--------------------
//
///*
// @method 当视频播放完毕释放对象
// */
//-(void)jonas:(NSNotification*)notify
//{
//    NSLog(@"fffffffffffdsfjkdsfjdskffj");
//    //视频播放对象
//    MPMoviePlayerController* theMovie = [notify object];
//    
//    //销毁播放通知
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:MPMoviePlayerPlaybackDidFinishNotification
//                                                  object:theMovie];
//    [theMovie.view removeFromSuperview];
//    // 释放视频对象
//}
//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
