//
//  IRPersonnelViewController.m
//  iResearch10
//
//  Created by jonas hu on 12-11-21.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import "IRPersonnelViewController.h"
#import "IRFoundation.h"
#import "IRPersonmentphotoViewController.h"
@implementation IRPersonnelViewController


@synthesize tableViewPersonnel;
@synthesize areaFile;
@synthesize depatment;
@synthesize linkManArray;




#pragma mark -表格数据源及代理


//行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return  linkManArray.count;
}




//每个Cell展现的内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString  *CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell==nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.selectionStyle=UITableViewCellSelectionStyleBlue;//选中Cell后背景
    }
    
    if([linkManArray count ]>indexPath.row)
    {
        cell.textLabel.text=linkManArray[indexPath.row];
        cell.imageView.image=[UIImage imageNamed:@"user.png"];
    }
    
    return cell;
}


//选中某一个Cell 要执行的事件
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([linkManArray count ]>indexPath.row)
    {
        
        IRPersonmentphotoViewController *personmentphotoViewController=[[IRPersonmentphotoViewController alloc] init];
        
        
        personmentphotoViewController.linkNameString=linkManArray[indexPath.row];
        personmentphotoViewController.hidesBottomBarWhenPushed=YES;
        
        [self.navigationController pushViewController:personmentphotoViewController animated:YES];

    }

    
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor=Color_BackgroundColor;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:areaFile
                                                     ofType:@"plist"];
    
    NSDictionary *areaLinkmanDic = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    
    linkManArray =[areaLinkmanDic objectForKey:depatment];
    
    self.navigationItem.title=[areaFile stringByAppendingString:depatment];
    
    
    //添加一个表格
    
    tableViewPersonnel= [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, ViewHeight) style:UITableViewStyleGrouped];
    
    tableViewPersonnel.delegate=self;
    
    tableViewPersonnel.dataSource=self;
    //tableViewCataLog.separatorColor=[UIColor clearColor];  //表间距背景色
    tableViewPersonnel.backgroundColor=[UIColor clearColor];
    tableViewPersonnel.backgroundView=nil;
    
    [self.view addSubview:tableViewPersonnel];



}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
