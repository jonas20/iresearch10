//
//  IROneViewController.h
//  iResearch10
//
//  Created by jonas hu on 12-11-21.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "XLCycleScrollView.h"

@interface IROneViewController : UIViewController<XLCycleScrollViewDatasource,XLCycleScrollViewDelegate>

//@property(nonatomic,strong)MPMoviePlayerController *theMovie;


@end
