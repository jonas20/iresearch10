//
//  IRGetImageFilename.h
//  iResearch10
//
//  Created by jonas hu on 12-11-22.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IRGetImageFilename : NSObject


+(NSArray *) getFilenamelistOfType:(NSString *)type fromDirPath:(NSString *)dirPath;

+(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;


@end
