//
//  ViewController.h
//  PSCollectionViewDemo
//
//  Created by Eric on 12-6-14.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSCollectionView.h"
#import "PullPsCollectionView.h"
#import "MBProgressHUD.h"


@interface IRViewController : UIViewController<PSCollectionViewDelegate,PSCollectionViewDataSource,UIScrollViewDelegate,PullPsCollectionViewDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,MBProgressHUDDelegate,UINavigationControllerDelegate>
{
    MBProgressHUD *HUD;

    int pages;
}


@property(nonatomic,retain) PullPsCollectionView *collectionView;
@property(nonatomic,retain)NSMutableArray *items;
@property(nonatomic,retain)NSMutableArray *itemsMore;
@property (nonatomic, retain) UIImagePickerController *imagePicker;


-(void)loadDataSource;
@end
