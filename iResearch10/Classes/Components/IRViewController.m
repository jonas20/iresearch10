//
//  ViewController.m
//  PSCollectionViewDemo
//
//  Created by Eric on 12-6-14.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "IRViewController.h"
#import "IRFoundation.h"
#import "PSCollectionViewCell.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import "Reachability.h"
#import "IRGetImageFilename.h"
#import "UIImageView+WebCache.h"



#import "CellView.h"
#import "UIImageView+WebCache.h"

#import "ASIFormDataRequest.h"

//#import "ASIHTTPRequest.h"
#import "IRInfoViewController.h"


static NSString *imagePath=@"ImageFile";



@implementation IRViewController
@synthesize collectionView;
@synthesize items;

@synthesize imagePicker;


#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //[self performSelector:@selector(saveImage:) withObject:image afterDelay:0.5];
    HUD = [[MBProgressHUD alloc] initWithWindow:[UIApplication sharedApplication].keyWindow];
    
	// Add HUD to screen
	[picker.view addSubview:HUD];
    
	// Regisete for HUD callbacks so we can remove it from the window at the right time
	HUD.delegate = self;
    
	HUD.labelText = @"正在上传...";
    
    
    /*
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    UIImage *originalImage, *editedImage, *imageToSave;
    
    // 处理静态照片
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0) == kCFCompareEqualTo) {
        
        editedImage = (UIImage *) [info objectForKey:UIImagePickerControllerEditedImage];
        originalImage = (UIImage *) [info objectForKey:UIImagePickerControllerOriginalImage];
        if (editedImage) {
            imageToSave = editedImage;
        }
        else {
            imageToSave = originalImage;
        }
    }
     */
    
    [HUD showWhileExecuting:@selector(imageHandle:) onTarget:self withObject:info animated:NO];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissModalViewControllerAnimated:YES];
}




#pragma -mark 自定义方法


-(void)UploadPhoto:(NSString *)imagePath
{
//    NSString *connectionKind;
    // 测试连接可用性
    Reachability *hostReach = [Reachability reachabilityWithHostName:@"www.baidu.com"];
	// 判断连接类型
    switch ([hostReach currentReachabilityStatus]) {
            
		case NotReachable:
            return;
            
//			connectionKind = @"没有网络链接";
            
            break;
            
		default:
            break;
	}
    
    
    NSString *str = [[NSString alloc] initWithFormat:@"http://%@/iresearch10/response.aspx?type=2",Domain];
    
    
    NSURL *url = [NSURL URLWithString:str];
    
    ASIFormDataRequest *dRequest = [ASIFormDataRequest requestWithURL:url];
    
    [dRequest setFile:imagePath withFileName:[NSString stringWithFormat:@"my%dphoto.jpg",arc4random()%100 ]  andContentType:@"image/jpeg"
               forKey:@"photo"];
    
    
    [dRequest setTimeOutSeconds:60];
    [dRequest setDelegate:self];
    
    
	//[dRequest startAsynchronous];
    [dRequest startSynchronous];

    
    NSError *error = [dRequest error];
    
    
    if (!error) {
        
        NSLog(@"%@",error.description);
        
    }else{
        
        NSLog(@"%@",error.description);
        
    }
}


//上传照片
-(void)ISNetwork
{    
    
    NSString *connectionKind;
    // 测试连接可用性
    Reachability *hostReach = [Reachability reachabilityWithHostName:@"www.baidu.com"];
	// 判断连接类型
    switch ([hostReach currentReachabilityStatus]) {
            
		case NotReachable:
			connectionKind = @"没有网络链接";
            
            
			break;
		
		default:
            //[self upLoadphtotgo];
            return;
			break;
	}
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"网络链接类型"
                                                    message:connectionKind
                                                   delegate:self
                                          cancelButtonTitle:@"知道了，谢谢"
                                          otherButtonTitles:nil];
    [alert show];
    
    
}

//图片处理
-(void)imageHandle:(NSDictionary *)info
{
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    UIImage *originalImage, *editedImage, *imageToSave;
    
    // 处理静态照片
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0) == kCFCompareEqualTo) {
        
        editedImage = (UIImage *) [info objectForKey:UIImagePickerControllerEditedImage];
        originalImage = (UIImage *) [info objectForKey:UIImagePickerControllerOriginalImage];
        if (editedImage) {
            imageToSave = editedImage;
        }
        else {
            imageToSave = originalImage;
        }
    }

    
    // 将静态照片（原始的或者被编辑过的）保存到相册（Camera Roll）
    //UIImageWriteToSavedPhotosAlbum (imageToSave, nil, nil , nil);
    // 根据图片控件的尺寸缩放照片（只是为了显示效果。实际传输时依然使用原始照片）
    
    CGSize imageSize=CGSizeMake(320, 428);
    
    if(imageToSave.size.width>imageToSave.size.height)
    {
         imageSize=CGSizeMake(320, 239);
    }
    
    UIImage* scaledImage = [IRGetImageFilename imageWithImage:imageToSave scaledToSize:imageSize];
    
    
    //以下是保存文件到沙盒路径下
    if (scaledImage != nil) {
        
        //把图片转成NSData类型的数据来保存文件
        NSData *data;
        //判断图片是不是png格式的文件
        if (UIImagePNGRepresentation(scaledImage)) {
            //返回为png图像。
            data = UIImagePNGRepresentation(scaledImage);
        }else {
            //返回为JPEG图像。
            data = UIImageJPEGRepresentation(scaledImage, 1.0);
        }
        
        //获取Documents文件夹目录
        NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentPath = [path objectAtIndex:0];
        //指定新建文件夹路径
        NSString *imageDocPath = [documentPath stringByAppendingPathComponent:imagePath];
        //创建ImageFile文件夹
        [[NSFileManager defaultManager] createDirectoryAtPath:imageDocPath withIntermediateDirectories:YES attributes:nil error:nil];
        
        //取好文件名
       // NSString *docs = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] ;
        
        //這句就是在上面獲取的路徑下加上你创建的文件夹的路径  类似xxx/xxxx/xxx/Documents/subdir/
        
        
      //  NSString *subdir = [docs stringByAppendingPathComponent:imagePath];//subdir就是你创建的文件夹名
        
        
        
        
        
        NSString *imagefilename=[[NSString alloc] initWithFormat:@"/image.jpg"];
        
        
        NSString *imagePath=[imageDocPath stringByAppendingPathComponent:imagefilename];
        
        //保存
        [[NSFileManager defaultManager] createFileAtPath:imagePath contents:data attributes:nil];
        
        [self UploadPhoto:imagePath];

    }
    
    
   // [self performSelectorOnMainThread:@selector(_demoAsyncDataLoading) withObject:nil waitUntilDone:NO];
    
    
    [imagePicker dismissModalViewControllerAnimated:NO];

    
    [self.items removeAllObjects];
    pages=0;
    
    [self loadDataSource];
    [self.collectionView reloadData];

    
}

-(void)barPhotoButtonItemClick:(UIBarButtonItem *)sender
{
    /*
     UIImagePickerController *pc = [[UIImagePickerController alloc]init];
     pc.delegate = self;
     pc.allowsEditing = NO;
     //pc.allowsImageEditing = NO;
     pc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
     [self presentModalViewController:pc animated:NO];
     */
    
    UIActionSheet *myActionSheet = [[UIActionSheet alloc]
                                    initWithTitle:nil
                                    delegate:self
                                    cancelButtonTitle:@"取消"
                                    destructiveButtonTitle:nil
                                    otherButtonTitles: @"从相册选择", @"拍照",nil];
    
    [myActionSheet showInView:self.view.window];
    
}

//从相册选择
-(void)LocalPhoto{
    
    imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = NO;
    //pc.allowsImageEditing = NO;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentModalViewController:imagePicker animated:NO];
    
}

//拍照
-(void)takePhoto{
    
    //资源类型为照相机
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    //判断是否有相机
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]){
        imagePicker= [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        //设置拍照后的图片可被编辑
        imagePicker.allowsEditing = NO;
        //资源类型为照相机
        imagePicker.sourceType = sourceType;
        [self presentModalViewController:imagePicker animated:YES];
    }else {
        NSLog(@"该设备无摄像头");
    }
}


-(void)hudWasHidden {
    
	// Remove HUD from screen when the HUD was hidded
	[HUD removeFromSuperview];
}

#pragma -mark actionSheet 代理
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            //从相册选择
            [self LocalPhoto];
            break;
        case 1:
            //拍照
            [self takePhoto];
            break;
        default:
            break;
    }
}



#pragma -mark 生命周期管理

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.items = [NSMutableArray array];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
//
//    
//    //    //这句是获取你的完整目录 就是从根目录一直到Documents目录下的路径   类似xxx/xxxx/xxx/Documents/
//    NSString *docs = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] ;
//    
//    //    //這句就是在上面獲取的路徑下加上你创建的文件夹的路径  类似xxx/xxxx/xxx/Documents/subdir/
//    NSString *subdir = [docs stringByAppendingPathComponent:@"ImageFile"];//subdir就是你创建的文件夹名
//    
//
//    
//    
//    NSString *imageFileString=[[NSString alloc]  initWithFormat:@"/%d.jpg",5];
//
//    UIImage *userPhotoImage = [UIImage imageWithContentsOfFile:[subdir stringByAppendingString:imageFileString]];
//
//    
//    NSLog(@"%f",userPhotoImage.size.width);
//    
//
//    [self Uploadphoto:[UIImage imageNamed:@"1.jpg"]];
//
//    return;
//
    
    self.view.backgroundColor=[UIColor blackColor];
    
    //添加拍照button
    
    UIBarButtonItem *barPhotoButtonItem=[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(barPhotoButtonItemClick:)];
    
    [self.navigationItem setRightBarButtonItem:barPhotoButtonItem animated:YES];
    
    
    
    collectionView = [[PullPsCollectionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:collectionView];
    collectionView.collectionViewDelegate = self;
    collectionView.collectionViewDataSource = self;
    collectionView.pullDelegate=self;
    collectionView.backgroundColor = [UIColor clearColor];
    collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    
    collectionView.numColsPortrait  = 3;
    collectionView.numColsLandscape =3;
    
    collectionView.pullArrowImage = [UIImage imageNamed:@"blackArrow"];
    collectionView.pullBackgroundColor = Color_NavigationBarTint;//[UIColor brownColor];
    collectionView.pullTextColor = [UIColor blackColor];
    
//    UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 45)];
//    [headerView setBackgroundColor:[UIColor redColor]];
//    self.collectionView.headerView=headerView;
    
//    UILabel *loadingLabel = [[UILabel alloc] initWithFrame:self.collectionView.bounds];
//    loadingLabel.text = @"请稍后...";
//    loadingLabel.textAlignment = UITextAlignmentCenter;
//    collectionView.loadingView = loadingLabel;
    
//    [self loadDataSource];
    
    
    if(!collectionView.pullTableIsRefreshing) {
        
        collectionView.pullTableIsRefreshing = YES;
        [self performSelector:@selector(refreshTable) withObject:nil afterDelay:0];
    }

}

- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
}

- (void)viewDidUnload
{
    [self setCollectionView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}
#pragma mark - PullTableViewDelegate

- (void) refreshTable
{
    /*
     
     Code to actually refresh goes here.
     
     */
    
    [self.items removeAllObjects];
    pages=0;
    
    [self loadDataSource];
    self.collectionView.pullLastRefreshDate = [NSDate date];
    self.collectionView.pullTableIsRefreshing = NO;
    [self.collectionView reloadData];
}

- (void) loadMoreDataToTable
{
    /*
     
     Code to actually load more data goes here.
     
     */
    
   //[self loadDataSource];

    
//    [self.items addObjectsFromArray:self.items];
    
    pages++;
    
    [self loadDataSource];

    [self.collectionView reloadData];
    self.collectionView.pullTableIsLoadingMore = NO;
}

- (void)pullPsCollectionViewDidTriggerRefresh:(PullPsCollectionView *)pullTableView
{
    [self performSelector:@selector(refreshTable) withObject:nil afterDelay:1.0f];
}



- (void)pullPsCollectionViewDidTriggerLoadMore:(PullPsCollectionView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:1.0f];
}

- (PSCollectionViewCell *)collectionView:(PSCollectionView *)collectionView viewAtIndex:(NSInteger)index {
    
    
//    NSDictionary *item = [self.items objectAtIndex:index];
    
    // You should probably subclass PSCollectionViewCell
    CellView *v = (CellView *)[self.collectionView dequeueReusableView];
//    if (!v) {
//        v = [[[PSCollectionViewCell alloc] initWithFrame:CGRectZero] autorelease];
//    }
    if(v == nil) {
        NSArray *nib =
        [[NSBundle mainBundle] loadNibNamed:@"CellView" owner:self options:nil];
        v = [nib objectAtIndex:0];
    }

    
//    [v fillViewWithObject:item];
   
//    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://imgur.com/%@%@", [item objectForKey:@"hash"], [item objectForKey:@"ext"]]];
    

    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/iresearch10/photo/%@",Domain,[self.items objectAtIndex:index]]];
//
//    
//    NSLog(@"%@",URL);
//    
    
   // NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.2.41:8086/iresearch10/photo/1.jpg"]];
    
    //    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    
    [v.picView  setImageWithURL:URL placeholderImage:[UIImage imageNamed:@"placeholder"]];
    v.picView.contentMode=UIViewContentModeScaleAspectFit;
    
    v.backgroundColor=[UIColor blackColor];
    return v;
}

- (CGFloat)heightForViewAtIndex:(NSInteger)index {
    NSDictionary *item = [self.items objectAtIndex:index];
    
    // You should probably subclass PSCollectionViewCell
    return [PSCollectionViewCell heightForViewWithObject:item inColumnWidth:self.collectionView.colWidth];
}

- (void)collectionView:(PSCollectionView *)collectionView didSelectView:(PSCollectionViewCell *)view atIndex:(NSInteger)index {
    
    
    IRInfoViewController *viewController=[[IRInfoViewController alloc] init];
    
    viewController.view=[NSKeyedUnarchiver unarchiveObjectWithData:[NSKeyedArchiver archivedDataWithRootObject:view]];
    viewController.hidesBottomBarWhenPushed=YES;
    viewController.view.backgroundColor=[UIColor blackColor];
    
    NSString *fileName=items[index];
    if([fileName hasPrefix:@"movie"])
    {
        viewController.isMovie=YES;
    }
    else
    {
        viewController.isMovie=NO;
    }
    viewController.fileName=fileName;
    
    [self.navigationController pushViewController:viewController animated:YES];
    
    
    NSLog(@"fds");
    // Do something with the tap
}

- (NSInteger)numberOfViewsInCollectionView:(PSCollectionView *)collectionView {
    return [self.items count];
}

- (void)loadDataSource {
    
//    [self.items removeAllObjects];

    
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"http://%@/iresearch10/response.aspx?type=1&page=%d" ,Domain,pages]];
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    
    [request startSynchronous];
    
    NSError *error = [request error];
    
    if (!error) {
        
        NSString *response = [request responseString];
        
        NSArray *imageArray=[response componentsSeparatedByString:@","];
        
        for (int i=0; i<imageArray.count; i++) {
            
            [items addObject:imageArray[i]];
            
        }
  
        [self dataSourceDidLoad];
        
    }
    else
    {
        [self dataSourceDidError];

    }
    
    return;

    /*
    // Request
    NSString *URLPath = [NSString stringWithFormat:@"http://imgur.com/gallery.json"];
    NSURL *URL = [NSURL URLWithString:URLPath];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        NSInteger responseCode = [(NSHTTPURLResponse *)response statusCode];
        
        if (!error && responseCode == 200) {
            id res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            if (res && [res isKindOfClass:[NSDictionary class]]) {
                
              //  self.items = [res objectForKey:@"data"];
                [items addObject:@"fds"];

                [self dataSourceDidLoad];
            } else {
                [self dataSourceDidError];
            }
        } else {
            [self dataSourceDidError];
        }
    }];
    */
    
    
}

- (void)dataSourceDidLoad {
    [self.collectionView reloadData];
}

- (void)dataSourceDidError {
    [self.collectionView reloadData];
}
@end
