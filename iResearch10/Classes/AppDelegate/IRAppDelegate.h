//
//  IRAppDelegate.h
//  iResearch10
//
//  Created by jonas hu on 12-11-21.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IRLoginViewController;

@interface IRAppDelegate : UIResponder <UIApplicationDelegate>


@property (strong, nonatomic) UIWindow *window;


@property(strong,nonatomic)IRLoginViewController *viewController;



@end
