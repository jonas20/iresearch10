//
//  IRFoundation.h
//  iResearch10
//
//  Created by jonas hu on 12-11-21.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#define Domain @"sysadmin.iusertracker.com"

//#define Domain @"192.168.2.41:8086"
//	======================================= 函数宏定义 ================================================
#define UTColor(r,g,b,a)[UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]

#define Color_BackgroundColor UTColor(250,250,250,1)
#define Color_NavigationBarTint UTColor(178,219,17,1)


//阅读设置背景
#define Color_ReadSettingBackground UTColor(235,235,235,0.1f)

//作者字体颜色
#define Color_AuthorFont UTColor(0,0,210,1)

#define Color_aboutTableBackground UTColor(225,224,222,1)

#define Color_blackColor UTColor(55,55,5,1)

#define Cell_hegit 130;

//视图高度 去掉导航视图和tab
#define ViewHeight [UIScreen mainScreen].bounds.size.height-44-49-20